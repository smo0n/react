import React, { useState } from 'react';
import './Composant/projet.css'
import LoginControl from './Composant/conditionnal/conditionnal';
import Counter from './Composant/counter/Counter';
import Multiform from './Composant/multiform/mutliform';
import Maxi from './Composant/max/max';
import Sum from './Composant/sum/sum';
import Graphic from './Composant/graphic/graphic'
import Welcome from './Composant/welcome/welcome';
import Facto from './Composant/facto/facto';


// BIEN LIRE LE README


function App(props) {

  let [nav, setNav] = useState("accueil");

  function handleClick(e) {
    let link = e.target.getAttribute("href").substr(1);
    setNav(link)
  }
  let exo;

  switch (nav) {

    case "Accueil":
      exo = <Welcome />;
      break;

    case "LoginControl":
      exo = <LoginControl />;
      break;

    case "Counter":
      exo = <Counter />;
      break;

    case "Multiform":
      exo = <Multiform />;
      break;

    case "Maximum":
      exo = <Maxi />;
      break;

    case "Somme":
      exo = <Sum />;
      break;


    case "Facto":
      exo = <Facto />;
      break;

    case "Graphique":
      exo = <Graphic />;
      break;

    default:
      exo = <Welcome />;

  }

  return (

    <div>
      <div className="nav">
        <a href="#Welcome" onClick={handleClick}>Accueil</a>
        <a href="#LoginControl" onClick={handleClick}>LoginControl</a >
        <a href="#Counter" onClick={handleClick}>Counter</a>
        <a href="#Multiform" onClick={handleClick}>Multiform</a>
        <a href="#Maximum" onClick={handleClick}>Maximum</a>
        <a href="#Somme" onClick={handleClick}>Somme</a>
        <a href="#Facto" onClick={handleClick}>Factorisation</a>
        <a href="#Graphique" onClick={handleClick}>Graphique</a>
      </div>
      {exo}

    </div>

  );

}

export default App;

