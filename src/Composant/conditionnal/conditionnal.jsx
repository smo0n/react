import React from 'react'
import '../projet.css'

function LoginControl(props) {

  function Inscrit(props) {
    return (
      <div>
        <h2>Exercice de Connexion/Déconnexion</h2>
        <p>Heureux de vous revoir</p>
      </div>

    )
  }

  function Invite(props) {
    return (
      <div>
        <h2>Exercice de Connexion/Déconnexion</h2>
        <p>Connectez-vous svp</p>
      </div>
    )
  }
  //Essaie d'une inscription mais sans succés. En effet, j'ai une erreur sur l'apparition du bouton
  // Le mot bouton n'apparait pas mais le nom du bouton apparait bien lors de la connexion. Pour eviter les erreurs.
  function Inscription() {
    return (
      <table>
        <tr>
          <td>Nom : </td>
          <td><input type="text" id="Nom" /></td>
        </tr>
        <tr>
          <td>Prénom : </td>
          <td><input type="text" id="Prénom" /></td>
        </tr>
        <tr>
          <td>Mot de passe : </td>
          <td><input type="text" id="mdp" /></td>
        </tr>
        <tr>
          <td>Validation Mot de passe : </td>
          <td><input type="text" id="vmdp" /></td>
        </tr>
        <tr>
          <td><button onClick={Inscrit}>Validé</button></td>
        </tr>
      </table>
    );

  }

  function Greeting(props) {
    const login = props.login;
    if (login) {
      return <Inscrit />;
    }
    return <Invite />;
  }

  function LoginButton(props) {
    function login() {
      props.SetisLogin()
    }
    return (
      <div>
        <button onClick={login}>Connexion</button><br />
      </div>
    );
  }

  function LogoutButton(props) {

    function logout() {
      props.SetisLogin()
    }
    return (
      <div>
        <button onClick={logout}>Deconnexion</button>
      </div>
    );
  }

  let [login, SetisLogin] = React.useState(false);
  let button;



  if (login) {
    button = <LogoutButton SetisLogin={() => SetisLogin(false)} />

  }
  else {
    button = <LoginButton SetisLogin={() => SetisLogin(true)} />
  }
  return (
    <div>
      <Greeting login={login} />
      {button}
    </div>
  );
}

export default LoginControl