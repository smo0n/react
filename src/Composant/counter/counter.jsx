import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import '../projet.css';
import { incrementation } from '../../Action/index';


function Counter(props) {

    //let count = 0
    let [count, setCount] = React.useState(0)
    let button = <button onClick={incremente}>Click Me</button>

    //redux
    let button2 = <button onClick={props.componentIncrement}>Click Me</button>

    function incremente() {
        setCount(count + 1)
    }


    return (
        <div>
            <h2>Exercice Counter :</h2>
            <p>You click {count} times</p>
            {button}<br /><br />

            <h2>Exercice Counter avec Redux :</h2>
            <p>You click {props.componentCount} times</p>
            {button2}<br /><br />
        </div>

    );
}

function mapStateToProps(state) {
    console.log(state);
    return {
        componentCount: state.count
    }
}

function mapDispatchToProps(dispatch) {
    return {
        componentIncrement: () => dispatch(incrementation())
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Counter)