import React from 'react';
import '../projet.css';

function Facto() {

  let [resultat, setResultat] = React.useState(0);
  let [valueOfInput, setValueOfInput] = React.useState()

  function factor() {
    let resultat = action(valueOfInput);
    setResultat(resultat);
  }

  function handleInputValue(event) {
    setValueOfInput(event.target.value);
  }

  function action(nbr) {
    if (nbr == 1) return 1;
    return nbr * action(nbr - 1);
  }

  return (
    <div><br /><br />
      <p>
        Votre Nombre: <input type="text" value={valueOfInput} onChange={handleInputValue} /><br /><br />
        <button onClick={factor}>Factoriel</button><br /><br />
        Le Factoriel est :  {resultat}
      </p>
    </div>
  )
}


export default Facto;

