import React from 'react';
import '../projet.css';
import { Bar } from 'react-chartjs-2'

//Pour que tout marche correctement, il faut installer les paquets react.js et react-chartjs-2
function Graphic() {


  return (

    <div>
      <p>Dans cette onglet, je vais vous faire un graphique qui va montrer la difficultés de chaque exercices et le nombre de personne qui a réussi cette exercice.</p>
      <div id="graph">
        <Bar
          data={{
            labels: ['LoginControl', 'Counter', 'Multifrom', 'Maximum', 'Somme', 'Factorisation'],
            datasets: [
              {
                label: 'Difficultés exercice',
                data: [12, 19, 7, 10, 5, 12],
                backgroundColor: [
                  'rgba(255, 99, 132, 0.8)',
                  'rgba(54, 162, 235, 0.8)',
                  'rgba(255, 206, 86, 0.8)',
                  'rgba(75, 192, 192, 0.8)',
                  'rgba(153, 102, 255, 0.8)',
                  'rgba(255, 159, 64, 0.8)'
                ],
                borderColor: [
                  'rgba(200, 99, 132, 1)',
                  'rgba(54, 162, 255, 1)',
                  'rgba(255, 250, 86, 1)',
                  'rgba(75, 192, 100, 1)',
                  'rgba(200, 102, 255, 1)',
                  'rgba(255, 200, 64, 1)'
                ],
                borderWidth: 3,
                hoverBorderColor: 'black',
              },
              {
                label: 'Nombre de personne ayant fait l exercice',
                data: [10, 10, 8, 5, 1, 4],
                backgroundColor: 'white',
                hoverBorderColor: 'black',
              }
            ]
          }}
          height={400}
          width={600}
          options={{
            maintainAspectRatio: false,
            title: {
              display: true,
              text: 'Difficultés des exercices et taux de réussite dans la classe',
              fontColor: 'black',
              fontSize: '18',
              position: 'bottom',
            },
            scales: {
              yAxes: [
                {
                  ticks: {
                    beginAtZero: true,
                  },
                },
              ],
            },
          }}
        ></Bar>
      </div>
    </div>



  );
}

export default Graphic;

