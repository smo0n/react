import React from 'react'
import '../projet.css'

function Maxi() {

    let [numberOne, setNumberOne] = React.useState();
    let [numberTwo, setNumberTwo] = React.useState();
    let [max, setMax] = React.useState();

    function calc() {
        let max = Math.max(numberOne, numberTwo)
        setMax(max)
    }

    return (
        <div>
            <h2>Exercice qui donne le chiffre le plus grand</h2>
            <p>Donnez vos 2 chiffres :</p>
            <p>Chiffre 1 : </p>
            <input type="text" value={numberOne} onChange={(event) => setNumberOne(event.target.value)} /><br />

            <p>Chiffre 2 : </p>
            <input type="text" value={numberTwo} onChange={(event) => setNumberTwo(event.target.value)} /><br /><br />

            <button onClick={calc}>Le plus grand</button>
            <p>Le chiffre le plus grand est : {max} </p>
        </div>

    );

}
export default Maxi;