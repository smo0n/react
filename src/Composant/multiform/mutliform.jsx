import React from 'react';
import '../projet.css'

function Multiform() {

    function multiplication() {
        let numberOne = document.getElementById("numberOne").value;
        let numberTwo = document.getElementById("numberTwo").value;
        document.getElementById("result").innerText = numberOne * numberTwo;
    }

    function division() {
        let numberOne = document.getElementById("numberOne").value;
        let numberTwo = document.getElementById("numberTwo").value;
        document.getElementById("result").innerText = numberOne / numberTwo;
    }

    return (
        <div>
            <h2>Exercice de Multiplication et Division :</h2>
            <table>
                <tr>
                    <td>Nombre 1: </td>
                    <td><input type="text" id="numberOne" /></td>
                </tr>
                <tr>
                    <td>Nombre 2 : </td>
                    <td><input type="text" id="numberTwo" /></td>
                </tr><br />
                <tr>
                    <td><button onClick={multiplication}>multiplication</button></td>
                    <td><button onClick={division}>division</button></td>
                </tr>
                <tr>
                    <td>
                        <span id="result"></span>
                    </td>
                </tr>
            </table>
        </div>

    )
}

export default Multiform;