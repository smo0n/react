import React from 'react';
import '../projet.css'

function Sum() {

    function Somme() {

        let numberThree = document.getElementById("numberThree").value;
        let numberFour = document.getElementById("numberFour").value;
        document.getElementById("result2").innerText = Number(numberThree) + Number(numberFour);
    }
    function Soustraction() {

        let numberThree = document.getElementById("numberThree").value;
        let numberFour = document.getElementById("numberFour").value;
        document.getElementById("result2").innerText = numberThree - numberFour;
    }

    return (

        <div>
            <h2>Exercice pour faire une adittion ou une soustraction :</h2>
            <table>
                <tr>
                    <td>Nombre 1: </td>
                    <td><input type="text" id="numberThree" /></td>
                </tr>
                <tr>
                    <td>Nombre 2: </td>
                    <td><input type="text" id="numberFour" /></td>
                </tr><br />
                <tr>
                    <td><button onClick={Somme}>Somme</button></td>
                    <td><button onClick={Soustraction}>Soustraction</button></td>
                </tr>
                <tr>
                    <td>
                        <span id="result2"></span>
                    </td>
                </tr>
            </table>
        </div>

    )
}

export default Sum;