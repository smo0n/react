import React, { useState } from 'react';
import '../projet.css';
import { Doughnut } from 'react-chartjs-2'

function Welcome() {

  // Pour l'exercice de la Clock, celui-ci est fonctionnel avec codepen. Néanmoins, j'ai essayé de reproduire l'exercice sur VSCode mais j'ai une erreur. 
  // Je laisse donc mon avancement même si il n y a pas le temps qui défile en continue.

  let [refresh, setRefresh] = useState(true);
  let date = new Date();

  function increment() {
    date.setMinutes(date.getMinutes + 1);
    setRefresh(true)
  }

  //Pour que tout marche correctement, il faut installer les paquets react.js et react-chartjs-2

  return (
    <div>
      <h1>Bonjour</h1>
      <p>Il est {date.toLocaleTimeString()}.</p><br />
      <p>Je me présente, je suis Monsieur Delerce Théo. Sur ce site, je vais vous présenter mes exercices effectués lors de mes séances de réact.
            Voici un graphique qui résume les exercice que j ai fait en fonction du temps que j ai mis : </p>

      <div id="graph">
        <Doughnut
          data={{
            labels: ['LoginControl', 'Counter', 'Multifrom', 'Maximum', 'Somme', 'Factorisation'],
            datasets: [
              {
                label: 'Temps en jours',
                data: [1, 2, 0.75, 1, 0.5, 1],
                backgroundColor: [
                  'rgba(255, 99, 132, 0.8)',
                  'rgba(54, 162, 235, 0.8)',
                  'rgba(255, 206, 86, 0.8)',
                  'rgba(75, 192, 192, 0.8)',
                  'rgba(153, 102, 255, 0.8)',
                  'rgba(255, 159, 64, 0.8)'
                ],
                borderColor: [
                  'rgba(200, 99, 132, 1)',
                  'rgba(54, 162, 255, 1)',
                  'rgba(255, 250, 86, 1)',
                  'rgba(75, 192, 100, 1)',
                  'rgba(200, 102, 255, 1)',
                  'rgba(255, 200, 64, 1)'
                ],
                borderWidth: 3,
                hoverBorderColor: 'black',
              },
            ]
          }}
          height={400}
          width={600}
          options={{
            maintainAspectRatio: false,
            title: {
              display: true,
              text: 'Exercice fait en fonction du temps passé',
              fontColor: 'black',
              fontSize: '18',
              position: 'bottom',
            },
            scales: {
              yAxes: [
                {
                  ticks: {
                    beginAtZero: true,
                  },
                },
              ],
            },
          }}
        ></Doughnut>
      </div>
    </div>
  );

}

setInterval(Welcome.increment, 1000);


export default Welcome;