import { INCREMENTATION } from '../Action/index';

const INITIAL_STATE = {
    count: 0
}

function counter(state = INITIAL_STATE, action) {
    console.log(state);
    switch (action.type) {
        case INCREMENTATION:
            return {
                count: state.count + 1
            }
        default:
            return state
    }

}
export default counter;